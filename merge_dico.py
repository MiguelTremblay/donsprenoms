#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Name:       merge_dico.py
Description:  Combine les dictionnaires de Retraite Québec afin de dénombrer le total des occurences
                     de chaque prénom, de trouver les prénoms communs pour les filles et les gars, 
                     d'écrire 3 fichiers: fille unique, gars unique, commun avec ratio fille/gars
                  

Notes:  https://framagit.org/MiguelTremblay/donsprenoms

Author: Miguel Tremblay
Date: 7 juin 2018
"""

import sys
import os


def main(sFillePath, sGarsPath, sRepertoireOutput):
    """
    Fonction qui fait des choses...
    """

    dFille = get_dico(sFillePath)
    dGars = get_dico(sGarsPath)

    setPrenomCommun = common_member(dFille.keys(), dGars.keys())

    sHeader = "Prénom, # Filles, # Gars, Fille vs Gars (%)\n"
    lCommun = []
    for sPrenom in setPrenomCommun:
        # Pourcentage fille vs gars, avec deux chiffres après la virgule
        fFilleVSGars = round(dFille[sPrenom]/(dFille[sPrenom]+dGars[sPrenom]),2)
        sLigne = sPrenom + "," + \
                 str(dFille[sPrenom]) + ","  +  str(dGars[sPrenom]) + \
                 "," + str(fFilleVSGars) 
        lCommun.append(sLigne)
        # Retirer les noms communs de la liste
        del dFille[sPrenom]
        del dGars[sPrenom]


    write_file(list(dFille.keys()), sRepertoireOutput, "fille.txt")
    write_file(list(dGars.keys()), sRepertoireOutput, "gars.txt")
    write_file(lCommun, sRepertoireOutput, "commun.txt", sHeader)

def write_file(lLignes, sRepertoire, sPathFichier, sPremiereLigne=None):
    """
    Ecriture du dictionnaire dans le fichier texte.
   """
               
    # Ecriture du fichier    
    if os.path.exists(sRepertoire) == False:
        print("ERREUR: Repertoire pour sortie n'existe pas: " + sRepertoire)
        exit(2)
    else:
        sFullPath = os.path.join(sRepertoire, sPathFichier)
        f = open(sFullPath, 'w')
        lLignes.sort()
        if sPremiereLigne != None:
            f.writelines(sPremiereLigne)
        f.writelines([sString+os.linesep for sString in lLignes])
        f.close()
      
def get_dico(sPath):
    """
    Ouvre le fichier CSV et crée un dictionnaire avec le prénom comme clé, et le 
    nombre d'occurences du prénom comme valeur.
    """
    
    # Ouverture du fichier Fille
    if os.path.exists(sPath) == False:
        print("ERREUR: Fichier CSV  n'existe pas: " +sPath)
        exit(2)
    else:
        file_list = open(sPath, 'r', encoding = "ISO-8859-1")
        lListString = file_list.readlines()
        file_list.close()

    dPrenomOccurence = {}
    for i in range(1,len(lListString)):
        lLigne = lListString[i].split(",")
        sPrenom  = lLigne[0]
        lsOccurence = lLigne[1:]
        lnOccurence = list(map(int, lsOccurence))
        nTotal = sum(lnOccurence)
        dPrenomOccurence[sPrenom] = nTotal

    return dPrenomOccurence


# Python program to find the common elements 
# in two lists
def common_member(a, b):
    """
    Copy from:
    https://www.geeksforgeeks.org/python-print-common-elements-two-lists/
    """
    a_set = set(a)
    b_set = set(b)
     
    # check length 
    if len(a_set.intersection(b_set)) > 0:
        return(a_set.intersection(b_set))  
    else:
        return None

if __name__ == "__main__":
    
   if len(sys.argv) < 4:
       print ("Usage merge_dico.py fille.csv gars.csv repertoire-sortie")
       sys.exit(1)

   sFille = sys.argv[1]
   sGars = sys.argv[2]
   sRepertoire = sys.argv[3]

   main(sFille, sGars, sRepertoire)

Identifier le genre du prénom des donateurs aux partis politiques du Québec
=============

Introduction
------------

`donsprenoms.py` est un script python 3 qui analyse les dons politiques du Québec, en les classant par parti politique, et permet de relier les prénoms des donateurs aux partis politiques du Québec selon le genre, en reliant le prénom à des listes de prénoms de filles et de garçons. Ces listes ont été créées, notamment, à l'aide de la [banque des prénoms de Retraite Québec](https://www.donneesquebec.ca/recherche/fr/organization/rrq) et des listes de prénoms sur Wikipédia.

Il est possible de consulter les fichiers d'analyses des dons politiques, par année, dans le répertoire [analyse-prenoms](/analyse-prenoms).  


Source des données 
------------

Prénoms des donateurs:
* [Donateurs aux partis politiques du Québec](https://www.electionsquebec.qc.ca/francais/provincial/financement-et-depenses-electorales/recherche-sur-les-donateurs.php)

Prénoms filles/gars:
* [Banque de prénoms des filles](https://www.donneesquebec.ca/recherche/fr/dataset/banque-de-prenoms-filles)
* [Banque de prénoms des garçons](https://www.donneesquebec.ca/recherche/fr/dataset/banque-de-prenoms-garcons)
 

### Listes des prénoms fournies avec donsprenoms

Les prénoms de la banque ont été combinés à l'aide du script `merge_dico.py`. Les prénoms ont ainsi été séparés en trois listes:
1. [Prénoms uniques aux filles](prenoms/filles-prenoms-uniques.txt) (169631 entrées, aucune occurrence de ces prénoms féminins dans la liste des prénoms des garçons)
2. [Prénoms uniques aux garçons](prenoms/gars-prenoms-uniques.txt) (164378 entrées, aucune occurrence de ces prénoms masculins dans la liste des prénoms des filles)
3. [Prénoms communs aux filles et garçons](prenoms/prenoms-gars-filles-communs.txt) (10778 entrées, au moins une occurrence dans chacune des listes)

Les autres listes de prénoms, telles que celles ayant dans le nom de fichier le mot 'grec' ou 'arabe' proviennent de Wikipédia. Par exemple:
1. [Liste de prénoms grecs](https://fr.wikipedia.org/wiki/Liste_de_pr%C3%A9noms_grecs)
2. [Liste de prénoms arabes](https://fr.wikipedia.org/wiki/Liste_de_pr%C3%A9noms_arabes)

### Licences des données

Bien que ce logiciel soit distribué avec la licence GPL version 3, les données provenant de Retraite Québec sont distribuées sous la licence [CC-BY](https://www.donneesquebec.ca/fr/licence/#cc-by) et celles d'Élection Québec sont sous [leur propre licence](http://dgeq.org/#license) (cliquer sur « Licence d'utilisation »). Les listes de prénoms supplémentaires (ex.: prénoms arabes) proviennent de Wikipédia et sont sous licence [CC-BY](https://www.donneesquebec.ca/fr/licence/#cc-by).

Méthodologie
------------

### Prénoms dans la banque de données de Retraite Québec

La colonne des prénoms de la liste des donateurs est utilisée pour identifier si la personne est un homme ou une femme. Lorsque le prénom est analysé, il y a 3 cas de figure qui peuvent survenir:
1. Le prénom est dans la liste des filles;
2. Le prénom est dans la liste des garçons;
3. Le prénom est dans la liste des filles et des garçons.

Bien que la liste des prénoms communs entre filles et garçons ne représente qu'environ 6% de tous les prénoms de la liste de Retraite Québec, 60% de tous les donateurs ont un prénom dans cette liste. Il est donc essentiel de répartir les donateurs ayant un prénom commun aux filles et garçons, car si les seuls 40% des donateurs ayant un prénom uniquement de fille ou de garçon étaient analysés, le résultat pourrait ne pas être représentatif de l'ensemble.

Ainsi, dans le cas de figure ou le prénom est une fille (1) ou un garçon (2), le donateur et le montant est ajouté à la liste correspondant au genre du prénom. Lorsque le prénom appartient à la fois à la liste des filles et des garçons, il est pondéré selon le nombre de prénoms qui a été donné à chacun des genres dans la liste des prénoms de Retraite Québec. Par exemple, le prénom 'Elli' ayant été donné à 19 filles et à 10 garçons, pour un ratio fille/garçon de 0,66, un donateur prénommé 'Elli' comptera pour 0,66 femme et 0,33 garçon. Le montant du don sera divisé selon le même ratio.

#### Cas spéciaux

Lorsqu'un prénom n'est pas textuellement dans la liste de retraite Québec, certains traitements sont appliqués pour aider à identifier le genre d'un prénom:
1. Les prénoms des donateurs se terminant ou débutant par une lettre majuscule seule suivie d'un point ont été simplifiés en retirant cette dernière. "Louise M." devient ainsi "Louise", "W. David" devient "David". La classification se fait par la suite sur le prénom résultant dans la liste de Retraite Québec.
2. Les prénoms des donateurs se terminant par une lettre majuscule seule précédée d'un tiret ont été simplifiés en retirant cette dernière. "Louis-T" devient ainsi "Louis". La classification se fait par la suite sur le prénom résultant dans la liste de Retraite Québec.
3. Les prénoms terminant par "Sr." ou "Jr.", avec toutes leurs variantes majuscules/minuscules, sont considérés comme étant des prénoms masculins.
4. Lorsqu'un prénom est composé de plusieurs prénoms, séparés par un tiret ou un espace, une moyenne des ratios filles/garçons est opérée sur la série de prénoms, lorsque ceux-ci sont dans la liste de Retraite Québec, ou sont ignorés lorsqu'ils en sont absents. Par exemple "Jenny-Marie", sera séparé en "Jenny" (ratio 0,99) et "Marie" (ratio 1.0) pour un ratio moyen de 0,995. De même, "Vouli Spyridoula" sera séparée en "Vouli" (non listé) et "Spyridoula" (dans la liste de prénom unique de fille), et sera considérée comme un prénom féminin.

### Prénoms absents de Retraite Québec

Plusieurs prénoms dans la liste de donateurs du DGEQ ne se trouvaient pas dans les listes de Retraite Québec. Afin d'avoir une classification la plus complète possible, une vérification manuelle de certains prénoms des donateurs a été effectuée. Les étapes suivantes pour identifier le genre d'un prénom a ont été utilisés:
1. Classement trivial (ex.: "Hughette")
2. Recherche sur le web pour voir s'il ne s'agissait pas d'un prénom typique à un genre (ex.: "[Minying](http://images.google.com/images?q=%minying)")
3. Trouver le nom de famille correspond au prénom dans le fichier du DGEQ, et tenter de trouver une photo de la personne par une recherche web (ex.: "[Nochane Rousseau](http://www.lapresse.ca/affaires/portfolio/developpement-minier/201604/25/01-4974709-une-annee-charniere-pour-plusieurs-mines.php)")

Les prénoms ainsi identifiés ont été classés dans les deux fichiers:
* [filles-non-listees.txt](prenoms/filles-non-listees.txt)
* [gars-non-listees.txt](prenoms/gars-non-listees.txt)

La liste complète des prénoms communs se trouve dans le fichier [prénoms communs aux filles et garçons](prenoms/prenoms-gars-filles-communs.txt). 

Ceux qui n'ont pas été classés sont dans le fichier [inconnus.txt](prenoms/inconnus.txt). Vous pouvez [contacter le développeur](mailto:donsprenoms.miguel@ptaff.ca) pour proposer de classer l'un de ces prénoms dans fille ou garçon, avec justification.

Résultats
------------

Les fichiers résultants de l'analyse se trouvent dans le répertoire [analyse-prenoms](analyse-prenoms/) et sont en format CSV. Une analyse manuelle supplémentaire a été effectuée, et contient notamment des graphiques, grâce au logiciel [gnumeric](http://www.gnumeric.org/).  Ce logiciel, d'un version égale ou supérieure à 1.12, doit être utilisé pour ouvrir les données avec l'extension _.gnumeric_. 

[Graphiques](graphiques/):
* [Montant et proportion de femmes ayant fait un don (2000-2018)](graphiques/proportion_femmes_dons_montants.png )
* [Proportion des femmes ayant fait des dons aux 4 principaux partis politiques au Québec (2000-2018)](graphiques/proportion_femmes_caq-plq-pq-qs.png )
* [Proportion des femmes ayant fait des dons aux partis politiques du Québec 2018 (jusqu'au 1 août)](graphiques/proportion_femmes_2018_caq-plq-pq-qs.png)

Dans les médias
------------

Couverture médiatique portant sur les résultats de ce script:
* [Beaucoup de femmes «poteaux» à la CAQ](https://www.journaldequebec.com/2018/08/31/beaucoup-de-femmes-poteaux-a-la-caq), Journal de Québec, 31 août 2018
* [Analyse des donateurs politiques selon le genre au Québec](http://ptaff.ca/blogue/2018/09/12/analyse_des_donateurs_politiques_selon_le_genre_au_qubec/), Hors des lieux communs, 12 septembre 2018


Prérequis
------------

* [python 3](https://www.python.org/downloads/)
* [unidecode](https://pypi.org/project/Unidecode/)
* [progress](https://pypi.python.org/pypi/progress)

Téléchargement
--------
La dernière version peut être téléchargée ici:<br>
https://framagit.org/MiguelTremblay/donsprenoms

La version git peut être téléchargée ici:<br>
 ```git clone https://framagit.org/MiguelTremblay/donsprenoms.git```
 

 Manuel
--------

De façon générale, ce logiciel devrait être appelé en ligne de commande de cette manière:
```bash
python3 donsprenoms.py -d FICHIER_DGEQ.csv -f filles-prenoms-uniques.txt -g gars-prenoms-uniques.txt -c prenoms-gars-filles-communs.txt
```


| Options                                  | Description |
| -------                                  | ------------|
| `-h`, `--help`                           | Afficher ce message d'aide et quitter.|
| `-o` `--fichier-sortie`&nbsp;CHEMIN                     |Fichier de sortie CSV où seront inscrit les résultats de l'analyse. Si aucune valeur n'est indiquée, le résultat est inscrit dans le terminal|
| `-d` `--DGEQ`&nbsp;CHEMIN                     | Fichier CSV provenant du site du DGEQ contenant les noms et montants des donateurs à un ou des partis politiques|
| `-f` `--Fille`&nbsp;CHEMIN1&nbsp;CHEMIN2&nbsp;[...]     | Fichier(s) contenant les prénoms des filles (un prénom par ligne)|
| `-g` `--Gars`&nbsp;CHEMIN1&nbsp;CHEMIN2&nbsp;[...]      | Fichier(s) contenant les prénoms des gars (un prénom par ligne)|
| `-c` `--Commun`&nbsp;CHEMIN              |Fichier contenant les prénoms communs aux filles et garçons (prénom, ration f/g)|
| `-i` `--inconnu`               | Afficher les prénoms inconnus|¸
| `-s` `--limite-sup` Montant ($)          |  Compter seulement les dons au-dessous de ce montant
| `-I ` `--limite-inf` Montant ($)          |  Compter seulement les dons au-dessus de ce montant
|`-v` `--verbose`                          | Expliquer ce qui se passe.|
|`-V` `--version`                          | Écrire l'information sur la version et quitter.|

Utilisation
-----

Pour utiliser avec les fichiers qui sont fournis avec le code:
```bash
python3 donsprenoms.py -d FICHIER_DGEQ.csv -f prenoms/filles-prenoms-uniques.txt prenoms/filles-non-listees.txt -g prenoms/gars-prenoms-uniques.txt prenoms/gars-non-listees.txt -c prenoms/prenoms-gars-filles-communs.txt -o analyse-prenoms/donateurs.csv
```

Sortie
-----

Les informations suivantes sont extraites du fichier (F = Femmes, H = Hommes, I = Inconnu ):
* Année
* Parti politique
* Nbr donateurs
* Proportion donateurs (%)
* Proportion argent (%)
* Montant total
* Montant moyen
* Nbr F
* Montant F
* Montant moyen F
* Proportion donateurs F
* Proportion montants F
* Nbr H
* Montant H
* Montant moyen H
* Proportion donateurs H
* Proportion montants H
* Nbr I
* Montant I
* Montant moyen I
* Proportion donateurs I
* Proportion montant I


Bogues
-----

Si vous trouvez un bogue, vous pouvez le rapporter à [donsprenoms.miguel@ptaff.ca](mailto:donsprenoms.miguel@ptaff.ca)

Auteur
-----

[Miguel Tremblay](http://ptaff.ca/miguel/)

Licence
-----

Copyright © 2018 Miguel Tremblay.

donsprenoms.py est un logiciel libre; vous pouvez le redistribuer et/ou le modifier selon les termes de la GNU General Public License (Licence Publique Générale GNU) telle qu'elle a été publiée par la Free Software Foundation; soit la version 3 de la licence, soit (comme vous le souhaitez) toute version ultérieure.
